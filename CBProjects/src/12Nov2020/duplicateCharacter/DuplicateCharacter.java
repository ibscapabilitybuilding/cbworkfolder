package duplicateCharacter;

import java.util.HashMap;
import java.util.Map;

public class DuplicateCharacter {

	private static void printDuplicate(String checkDuplicate) {

		char[] singleChars = checkDuplicate.toCharArray();
		Boolean duplicates = false;

		Map<Character, Integer> charMap = new HashMap<>();

		for (char c : singleChars) {
			if (!charMap.isEmpty() && charMap.containsKey(c)) {
				charMap.put(c, charMap.get(c) + 1);
				duplicates = true;
			} else
				charMap.put(c, 1);
		}

		System.out.print("Duplicates characters - ");
		if (duplicates) {
			for (Map.Entry<Character, Integer> characterValue : charMap.entrySet()) {
				if (characterValue.getValue() > 1) {
					System.out.print(characterValue.getKey()+ " ");
				}
			}
		} else
			System.out.print(" not available in "+checkDuplicate);

	}

	public static void main(String args[]) {
		String checkDuplicate = "Checkanyduplicatechar";
		printDuplicate(checkDuplicate);

	}
}
