package nonRepeatedChar;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class FirstNonRepeatedCharacter {

	private static void findFirstNonRepeatedChar(String inputString) {

		char[] singleChars = inputString.toCharArray();
		Map<Character, Integer> charMap = new LinkedHashMap<>();

		for (char c : singleChars) {
			if (!charMap.isEmpty() && charMap.containsKey(c))
				charMap.put(c, charMap.get(c) + 1);
			else
				charMap.put(c, 1);
		}

		if (charMap.containsValue(1)) {
			for (Map.Entry<Character, Integer> characterValue : charMap.entrySet()) {
				if (characterValue.getValue() == 1 ) {
					System.out.print("First Unique character in "+inputString+ " is "+characterValue.getKey());
					break;
				}
			}
		} else {
			System.out.print(" No unique character present");
		}

	}

	public static void main(String args[]) {
		String inputString = "Checkfirstuniquecharacter";
		findFirstNonRepeatedChar(inputString.toLowerCase());

	}
}
