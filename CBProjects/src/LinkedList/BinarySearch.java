
public class BinarySearch {

	public static int binarySearch(int[] sortedArray, int target) {
	    int head, left = 0, right = sortedArray.length - 1;
	    while (left <= right) {
	      head = left + (right - left) / 2;
	      if (sortedArray[head] == target) return head;
	      if (target < sortedArray[head]) right = head - 1;
	      else left = head + 1;
	    }
	    return -1;
	  }
	
	public static void main(String[] args) {
		int[] sortedArray = {1,3,4,7,9,11,14,18,22};
		System.out.println(" Position of 14 : "+binarySearch(sortedArray,14));
	}
}
