
public class BinaryToInt {

	private static void binaryToInt(ListNode ln) {
		ListNode head = ln;
		int length = 0, number = 0;
		while (ln != null) {		
			length++;
			ln = ln.next;
		}
		System.out.print("Integer value of ");
		while(head != null) {
			number += (int) head.val*(Math.pow(2, length-1));
			System.out.print(" "+head.val);
			head = head.next;
			length--;
		}
		System.out.print("  : "+number);
		
	}

	public static void main(String[] args) {
		ListNode node = new ListNode(1, new ListNode(0, new ListNode(0, new ListNode(1, new ListNode(1, null)))));
		binaryToInt(node);
	} 
}
