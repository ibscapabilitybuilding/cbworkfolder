
public class AddNumbersLL {
	
	public static void addTwoNumbers(ListNode l1, ListNode l2) {
		int result = 0, temp = 0;
		ListNode total ;
		ListNode sum = new ListNode();

		while (l2 !=null || l1 !=null) {
			result = getValue(l1)+getValue(l2)+temp;
			if (result > 10) {
				temp = result / 10;
				result = result % 10;
			} else {
				temp = 0;
			}	
			total = new ListNode(result);
			total.next = sum;
			sum = total;  
			if (l1 != null)
				l1 = l1.next;
			if (l2 !=null)
				l2 = l2.next;
			if (l1 == null && l2 ==null && temp != 0) {
				total = new ListNode(temp);
				total.next = sum;
				sum = total;  
			}
		}
		
		System.out.print(" Result : ");
		while (sum.next != null) {
			System.out.print(sum.val);
			sum = sum.next;
		}
	}

	private static int getValue(ListNode ln) {
		if (ln != null )
			return ln.val;
		else
			return 0;
	}
	
	
	public static void main(String[] args) {
		ListNode l1 = new ListNode(1,new ListNode(6,new ListNode(8,new ListNode(1,null))));
		ListNode l2 = new ListNode(4,new ListNode(8,new ListNode(2,new ListNode(2,null))));
		addTwoNumbers(l1,l2);
	}

}
