
public class DeleteNodeLL {

	
	private static void deleteNode(ListNode ln,int number) {
		
			ListNode prv = ln;
			ListNode head = ln;
			while(ln != null ) {
				if (ln.val == number) {
					if ( ln.next != null) {
						ln.val = ln.next.val;
						prv.next = ln.next;
					} else {
						prv.next = null;
					}
					
				} else {
					prv = ln;
				}
				ln = ln.next;		
			}
			
			System.out.print("After deleting "+number+ " - ");	
			while(head != null ) {
				System.out.print(" "+head.val);	
				head = head.next;
			}
	}
	
	public static void main(String[] args) {
		ListNode node = new ListNode(1,new ListNode(6,new ListNode(8,new ListNode(1,new ListNode(3,null)))));
		deleteNode(node,3);
	}
	
}
