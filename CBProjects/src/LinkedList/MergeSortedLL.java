import java.util.Collections;
import java.util.LinkedList;

public class MergeSortedLL {

	public static void main(String[] args) {
		ListNode node1 = new ListNode(1, new ListNode(3, new ListNode(4,new ListNode(5, new ListNode(7, new ListNode(11, null))))));
		ListNode node2 = new ListNode(2, new ListNode(6, new ListNode(8,new ListNode(9, new ListNode(10, new ListNode(12, null))))));
		
		
		LinkedList<Integer> singlyLinkedList = new LinkedList<>();
		while(node1 != null) {
			singlyLinkedList.add(node1.val);
			node1=node1.next;
		}
		while(node2 != null) {
			singlyLinkedList.add(node2.val);
			node2=node2.next;
		}
		
		Collections.sort(singlyLinkedList);
		System.out.print("After sorting");
		for(Integer i : singlyLinkedList) {
			System.out.print(" "+i);
		}
	}
	
}
