
public class RemoveDuplicateSortedLL {

	private static void removeDuplicates(ListNode ln) {
		ListNode head = ln;
		ListNode first = new ListNode(0);
		first.next = head;
		ListNode prev = first;
		ListNode current = head;

		while (current != null) {
			
			while (current.next != null && prev.next.val == current.next.val)
				current = current.next;

			if (prev.next == current)
				prev = prev.next;
			else
				prev.next = current.next;

			current = current.next;
		}
		head = first.next;
		System.out.print("After Deletion - ");
		while (head != null) {
			System.out.print(" " + head.val);
			head = head.next;
		}

	}

	public static void main(String[] args) {
		ListNode node = new ListNode(1, new ListNode(3, new ListNode(4,
				new ListNode(5, new ListNode(5, new ListNode(6, new ListNode(7, new ListNode(7, null))))))));
		removeDuplicates(node);
	}
}
