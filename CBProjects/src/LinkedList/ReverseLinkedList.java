
public class ReverseLinkedList {

	public static void reverseList(ListNode node) {
		
		ListNode prev = null;
		ListNode nxt;
		ListNode current = node;
		System.out.print("Linked List - ");
		while (current != null ) {
			System.out.print(" " +current.val);
			nxt = current.next;
			current.next = prev;
			prev = current;
			current = nxt;
		}
		node = prev;
		
		System.out.print("\nAfter Reversing Linked List - ");
		while (node != null) {
			System.out.print(" " +node.val);
			node = node.next;
		}
	}

	
	public static void main(String[] args) {
		ListNode l1 = new ListNode(1,new ListNode(6,new ListNode(8,new ListNode(1,null))));
		reverseList(l1);
	}
}
