import java.util.ArrayList;
import java.util.List;

public class NextGreatest {
	private static void findNextGreatest(ListNode ln) {

		int[] newNode = new int[10];
		ListNode head = ln;
		int number = head.val, count = 0;
		
			
			List<Integer> result = new ArrayList<>();
	        while (head != null) {
	            ListNode cur = head;
	            while (head != null && head.val <= cur.val) {
	                head = head.next;
	            }
	 
	            if (head != null) {
	                result.add(head.val);
	            } else {
	                result.add(0);
	            }
	            head = cur.next;
	        }
	        
	        int[] nums = new int[result.size()];
	        for (int i = 0; i < result.size(); i++) {
	            nums[i] = result.get(i);
	        }

		
		
		for(int i : nums) {
			System.out.print(i);
		}
		
	}

	public static void main(String[] args) {
		ListNode node = new ListNode(1, new ListNode(7, new ListNode(5, new ListNode(1, new ListNode(9, new ListNode(2,  new ListNode(5,  new ListNode(1, null))))))));
		findNextGreatest(node);
	}
}
