
public class PartitionList {

public static void partitionList(ListNode node, int number) {
		
		ListNode head = node;
		ListNode before = null, after = null , afterStart = null , start = null;
		System.out.print("Linked List - ");
		while (head != null ) {
			System.out.print(" " +head.val);
		
			if (head.val >= number) {
				if (after == null) {
					after = new ListNode();
					afterStart = after;
				} else {
					after.next = new ListNode();
					after = after.next;
				}
				after.val = head.val;
			} else {
				if (before == null) {
					before = new ListNode();
					start = before;
				} else {
					before.next = new ListNode();
					before = before.next;
				}
				before.val = head.val;
				
			}
			head = head.next;
		}
		before.next = afterStart;
		
		System.out.print("\nAfter Partitioning  - ");
		while (start != null) {
			System.out.print(" " +start.val);
			start = start.next;
		}
	}

	
	public static void main(String[] args) {
		ListNode l1 = new ListNode(1,new ListNode(6,new ListNode(8,new ListNode(3,new ListNode(5,new ListNode(2,null))))));
		partitionList(l1,4);
	}
}
