
public class SubStringCheck {

	private static void checkSubString(String inputStringOne, String inputStringTwo) {
		  
		  if (inputStringOne.contains(inputStringTwo) ) {
			  System.out.print("' "+inputStringTwo + "' is sub string of '"+inputStringOne+"' ");
		  } else if (inputStringTwo.contains(inputStringOne)) {
			  System.out.print("' "+inputStringOne + "' is sub string of '"+inputStringTwo+"' ");
		  } else 
			  System.out.print("No substrings present");
		}
		
		public static void main(String args[]) {
			String inputStringOne = "Check any string contains my words";
			String inputStringTwo = "contains";
			checkSubString(inputStringOne,inputStringTwo);

		}
		
}
