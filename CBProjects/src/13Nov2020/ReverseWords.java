
public class ReverseWords {

	private static void reverseWords(String inputString) {
	  String[] splittedArray = inputString.split(" ");
	  
	  System.out.print("String reverse of "+inputString+ " - ");
	  for (int i=splittedArray.length-1 ; i >= 0 ; i--)
		  System.out.print(" "+splittedArray[i]);
	}
	
	public static void main(String args[]) {
		String inputString = "String reverse the is This";
		reverseWords(inputString);

	}
	
}
