
public class StringReverse {

	private static void reverseString(String inputString) {
		  
		  System.out.print("String reverse of "+inputString+ " - ");
		  for (int i=inputString.length()-1 ; i >= 0 ; i--)
			  System.out.print(" "+inputString.charAt(i));
		}
		
		public static void main(String args[]) {
			String inputString = "startandend";
			reverseString(inputString);

		}
		
}
