import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class DiagonalSort {

	public static int[][] diagonalSort(int[][] mat) {
		int m = 5, n = 5;
		int[][] sortedMatrix = new int[m][n];
		
		System.out.println(" Matrix to be sorted");
		for (int i = 0; i <= m - 1; i++) {
			for (int j = 0; j <= (n - 1); j++) {
				System.out.print(" " + mat[i][j]);
			}
			System.out.println();
		}
		 
		
		List<Integer> diagonalSet = null;
		for (int s=0; s<m; s++) { 
			diagonalSet = new ArrayList();
		
		    for (int i=s; i>-1; i--) { 
		        diagonalSet.add(mat[i][s-i]);
		    } 
		    Collections.sort(diagonalSet);
		    int len = 0;
		    for (int i=s; i>-1; i--) { 
		    	mat[i][s-i]=diagonalSet.get(len);        
		        len++;
		    } 
		    
		} 
		
		         
		for (int s=1; s<n; s++) { 
			diagonalSet = new ArrayList();
		    for (int i=n-1; i>=s; i--) { 
		        diagonalSet.add(mat[i][s+n-1-i]);
		    } 	    
		    Collections.sort(diagonalSet);
		    int len = 0;
		    for (int i=n-1; i>=s; i--) { 
		        mat[i][s+n-1-i] = diagonalSet.get(len);
		        len++;
		    } 
		} 
		
		

		/*
		 * int l=0; int k=0; for (int i = n-1; i>=0; i--) { l=0; for(int
		 * j=0,col=0;j<=col;j++) { if (col<=k && l <= n-1) { col++;
		 * 
		 * System.out.print("   "+(i+l) + " " + (j) + " : " + mat[i+l][j]); l++; }
		 * 
		 * } if(k<=n-1) k++; System.out.println(); }
		 */
		System.out.println(" Sorted Matrix");
		for (int i = 0; i <= m - 1; i++) {
			for (int j = 0; j <= (n - 1); j++) {
				System.out.print(" " + mat[i][j]);
			}
			System.out.println();
		}

		return null;
	}

	
	public static void main(String args[]) {

		//int a[][] = { { 2, 3, 4, 5 }, { 6, 1, 2, 3 }, { 3, 4, 5, 4 }, { 2, 1, 2, 4 } };
		int a[][] = { { 2, 3, 4, 5,1 },{ 3, 4, 5, 4,2 }, { 6, 1, 2, 3 ,1}, { 3, 4, 5, 4,2 }, { 2, 1, 2, 4,2 } };
		diagonalSort(a);

	}
}
