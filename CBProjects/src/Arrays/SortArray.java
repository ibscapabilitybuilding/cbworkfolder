import java.util.Arrays;
import java.util.stream.Stream;

public class SortArray {


	public static void main(String args[]) {
		Integer[] num1 = new Integer[]{ 1, 3, 5, 7, 9 };
		Integer[] num2 = new Integer[]{ 2, 4, 6, 8, 10 };
		Object[]  mergedArray = Stream.of(num1, num2).flatMap(Stream::of).toArray();
		Arrays.sort(mergedArray);
        System.out.println("Sorted Array ");
        for(int len=0; len < mergedArray.length; len++)
        	System.out.println(mergedArray[len]);
	}
}
