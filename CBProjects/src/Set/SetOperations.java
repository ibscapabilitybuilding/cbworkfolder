import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetOperations {

	public static void main(String[] args) {
		Set<Integer> dataSetFirst = new HashSet<>();
		dataSetFirst.add(1);
		dataSetFirst.add(2);
		dataSetFirst.add(3);
		
		Set<Integer> dataSetSecond = new HashSet<>();
		dataSetSecond.add(1);
		dataSetSecond.add(3);
		dataSetSecond.add(5);
		dataSetSecond.add(7);
		
		Integer[] toIntArray =  Arrays.copyOf(dataSetFirst.toArray(), dataSetFirst.size(), Integer[].class);
		System.out.print("After converting to array : ");
		for (Integer i : toIntArray) {
			System.out.print(" "+i);
		}
		
		dataSetFirst.retainAll(dataSetSecond);
		System.out.print("\nAfter retaining the common elements - ");
		for (Integer i : dataSetFirst) {
			System.out.print(" "+i);
		}
		
		dataSetFirst.clear();
		System.out.println("\nCurrent size after empty the set: "+dataSetFirst.size());
	}
}
