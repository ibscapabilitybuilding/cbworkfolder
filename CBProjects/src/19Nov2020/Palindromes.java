import java.util.HashSet;
import java.util.Set;

public class Palindromes {
	
	private static void palindromeSubstring(String str, int min, int max, Set<String> set)  {
		int length = str.length();
		while((min>=0 && max<length) && (str.charAt(min)==str.charAt(max))) {
			set.add(str.substring(min,max+1));
			min--;
			max++;
		}
	}

	private static void substringsOfPalindrome(String givenString) {
		String str = givenString;
		int strLength = str.length();
		Set<String> substringSet = new HashSet<>();
		for(int i=0;i<strLength;i++)  {
			palindromeSubstring(str,i,i,substringSet);

			palindromeSubstring(str,i,i+1,substringSet);
		}
		System.out.println(substringSet);
	}

	private static String substringPalindrome(String str, int min, int max)  {
		int length = str.length();
		while((min>=0 && max<length) && (str.charAt(min)==str.charAt(max))) {
			min--;
			max++;
		}
		return str.substring(min+1,max);
	}

	private static String findLongestPalindrome(String givenString) {
		String str = givenString;
		String maxString="", curString;
		int maxLength = 0, curLength;
		int strLength = str.length();
		for(int i=0;i<strLength-1;i++)  {
			curString = substringPalindrome(str,i,i);
			curLength = curString.length();
			if(curLength>maxLength) {
				maxString = curString;
				maxLength = curLength;
			}

			curString = substringPalindrome(str,i,i+1);
			curLength = maxString.length();
			if(curLength>maxLength) {
				maxString = curString;
				maxLength = curLength;
			}
		}
		return maxString;
	}

	public static void main(String[] args) {
		String givenString = "BIBIBNBIBIB";
		System.out.println("The palindrome substrings of given string " + givenString + " are : ");
		substringsOfPalindrome(givenString);
		System.out.println("The longest palindrome substring of given string " + givenString + " is : "+ findLongestPalindrome(givenString));
	}

}
