
public class OuterParanthesis {
	 public static void removeOuterParentheses(String S) {
		 
	        String result = "";
	        int openCount = 0;
	        for (int i=0; i< S.length(); i++) {
				
	            if (S.charAt(i) == '(' && openCount++ > 0) result += S.charAt(i);
	            if (S.charAt(i)  == ')' && openCount-- > 1) result += S.charAt(i);
	        }
	        System.out.println(" New String : "+result);
	    }
	    
			
			public static void main(String args[]) {
				String inputString = "(()())(())(()(()))";
				removeOuterParentheses(inputString);

			}
}
