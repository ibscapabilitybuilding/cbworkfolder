import java.util.AbstractMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class TreeMapOperations {

	public static void main(String args[]) {

		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("Key1", "Value1");
		treeMap.put("Key2", "Value2");
		treeMap.put("Key3", "Value3");
		treeMap.put("Key4", "Value4");

		System.out.println(treeMap);
		if (treeMap.containsKey("Key2")) {
			System.out.println("The Tree Map contains key ");
		} else {
			System.out.println("The Tree Map does not contain key");
		}
		
		System.out.println("Reverse Order");
		Map<String, String> reverseMap =  
	               new TreeMap<String, String>(Collections.reverseOrder()); 
		reverseMap.put("1", "this"); 
		reverseMap.put("2", "is"); 
		reverseMap.put("3", "going"); 
		reverseMap.put("4", "to be"); 
		reverseMap.put("5", "reverse");
		Set set = reverseMap.entrySet(); 
        Iterator i = set.iterator(); 
  
        while (i.hasNext()) { 
            Map.Entry me = (Map.Entry)i.next(); 
            System.out.print(me.getKey() + ": "); 
            System.out.println(me.getValue()); 
        } 
        
    	System.out.println("Sorting Keys using comparator"); 
    	TreeMap<Integer,String> trmap = new TreeMap<Integer, String>(new KeyComparator());
	    trmap.put(3, " ");
	    trmap.put(1, " ");
	    trmap.put(8, " ");
	    trmap.put(7, " ");
	    trmap.put(5, " ");
	    for (Entry<Integer, String> tr : trmap.entrySet()) {
	    	System.out.println(tr.getKey());
	    }
	    
		System.out.println("Finding greatest key less than or equal key of 4 - "+trmap.floorKey(4)); 
		
	    
		/*
		 * //By default tree map will display keys in sorted order TreeMap<Integer,
		 * String> treeMap1 = new TreeMap<Integer, String>(); treeMap1.put(3, " ");
		 * treeMap1.put(1, " "); treeMap1.put(8, " "); treeMap1.put(7, " ");
		 * treeMap1.put(5, " "); for (Entry<Integer, String> tr : treeMap1.entrySet()) {
		 * System.out.println(tr.getKey()); }
		 */
	}
}

class KeyComparator implements Comparator<Integer>{
	 
    @Override
    public int compare(Integer i1, Integer i2) {
        if(i1 > i2){
            return 1;
        } else {
            return -1;
        }
    }

}
